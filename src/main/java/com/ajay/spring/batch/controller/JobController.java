package com.ajay.spring.batch.controller;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/batch")
public class JobController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job; // Inject the specific job you want to run

    @Autowired
    private JobExplorer jobExplorer;


    @Autowired
    private Job importCustomerJob;

    /**
     * Endpoint to run the batch job.
     *
     * @return ResponseEntity indicating the job status.
     */
    @PostMapping("/run")
    public ResponseEntity<String> runJob() {
        try {
            JobParameters jobParameters = new JobParametersBuilder()
                    .addLong("time", System.currentTimeMillis())
                    .toJobParameters();
            jobLauncher.run(job, jobParameters);
            return ResponseEntity.ok("Batch job has been invoked successfully.");
        } catch (JobExecutionException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error running batch job: " + e.getMessage());
        }
    }

    /**
     * Endpoint to get the status of a specific job execution.
     *
     * @param jobExecutionId The ID of the job execution to check the status of.
     * @return ResponseEntity indicating the job execution status.
     */
    @GetMapping("/status/{jobExecutionId}")
    public ResponseEntity<String> getJobStatus(@PathVariable("jobExecutionId") Long jobExecutionId) {
        JobExecution jobExecution = jobExplorer.getJobExecution(jobExecutionId);
        if (jobExecution != null) {
            BatchStatus status = jobExecution.getStatus();
            return ResponseEntity.ok("Job Execution Status: " + status.toString());
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/run-batch-job")
    public ResponseEntity<String> runJob(@RequestParam(value = "jobName", defaultValue = "importCustomerJob") String jobName) {
        try {
            JobParameters jobParameters = new JobParametersBuilder()
                    .addLong("startAt", System.currentTimeMillis()).toJobParameters();
            jobLauncher.run(importCustomerJob, jobParameters);
            return ResponseEntity.ok("Batch job has been invoked");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Batch job failed: " + e.getMessage());
        }
    }
}
