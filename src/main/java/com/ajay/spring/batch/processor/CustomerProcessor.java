package com.ajay.spring.batch.processor;

import com.ajay.spring.batch.entity.Customer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

/**
 * CustomerProcessor processes each Customer item.
 * This is where you can add any custom processing logic for each Customer.
 */
@Component
public class CustomerProcessor implements ItemProcessor<Customer, Customer> {

    /**
     * Process the given customer and return the processed result.
     * You can add custom processing logic here.
     *
     * @param customer the customer to be processed
     * @return the processed customer
     * @throws Exception if any error occurs during processing
     */
    @Override
    public Customer process(Customer customer) throws Exception {
        // Example processing: convert names to uppercase
        customer.setFirstName(customer.getFirstName().toUpperCase());
        customer.setLastName(customer.getLastName().toUpperCase());

        // Example processing: trim spaces from email
        customer.setEmail(customer.getEmail().trim());

        // Add any additional processing logic here

        return customer;
    }
}
