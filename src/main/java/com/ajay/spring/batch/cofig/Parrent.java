package com.ajay.spring.batch.cofig;

public class Parrent {
    public static void main(String[] args) {
        Parrent parrent = new Parrent();
        parrent.sum(3, 4);
        parrent.sum(5, 6, 7);
    }

    public int sum(int a, int b) {
        return a + b;
    }

    public int sum(int a, int b, int c) {
        return a + b + c;
    }

}
