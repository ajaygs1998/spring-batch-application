package com.ajay.spring.batch.cofig;

import com.ajay.spring.batch.entity.Customer;
import com.ajay.spring.batch.processor.CustomerProcessor;
import com.ajay.spring.batch.repository.CustomerRepository;
import jakarta.persistence.EntityManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;


/**
 * Batch configuration class for setting up Spring Batch components.
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(BatchConfiguration.class);
    //private final JobBuilderFactory JobBuilderFactory;   //deprecated in the spring 5
    // private final StepBuilderFactory stepBuilderFactory; //deprecated in the spring 5
    private final CustomerRepository customerRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final CustomerProcessor customerProcessor;

    /**
     * Constructor injection of JobBuilderFactory, StepBuilderFactory, and CustomerRepository.
     *
     * @param customerRepository   CustomerRepository instance for data access
     * @param entityManagerFactory
     * @param customerProcessor
     */
    @Autowired
    public BatchConfiguration(CustomerRepository customerRepository, EntityManagerFactory entityManagerFactory, CustomerProcessor customerProcessor) {
        this.customerRepository = customerRepository;
        this.entityManagerFactory = entityManagerFactory;
        this.customerProcessor = customerProcessor;
    }

    /**
     * Creates a FlatFileItemReader bean for reading customer data from a CSV file.
     *
     * @return a configured FlatFileItemReader.
     */
    @Bean
    public FlatFileItemReader<Customer> reader() {
        FlatFileItemReader<Customer> itemReader = new FlatFileItemReader<>();
        itemReader.setResource(new FileSystemResource("src/main/resources/customers-1000.csv"));
        itemReader.setName("csvReader");
        itemReader.setLinesToSkip(1);
        itemReader.setLineMapper(this.lineMapper());
        return itemReader;
    }

    private LineMapper<Customer> lineMapper() {
        DefaultLineMapper<Customer> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("id", "firstName", "lastName", "company", "city", "phone1", "phone2", "email", "subscriptionDate", "website", "phoneNumber");

        BeanWrapperFieldSetMapper<Customer> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Customer.class);
        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        return lineMapper;
    }


    /**
     * Bean configuration for JpaItemWriter.
     * This writer writes customer data to the database.
     *
     * @return JpaItemWriter<Customer>
     */
    @Bean
    public RepositoryItemWriter<Customer> writer() {
        RepositoryItemWriter<Customer> writer = new RepositoryItemWriter<>();
        writer.setMethodName("save");
        writer.setRepository(customerRepository);
        return writer;
    }


    /**
     * Bean configuration for the job.
     * This job processes the customer data.
     *
     * @param listener the job completion listener
     *                 step1()    the step to be executed as part of the job
     * @return Job
     */

    @Bean
    public Job importCustomerJob(JobRepository jobRepository, JobExecutionListener listener, PlatformTransactionManager transactionManager) {
        return new JobBuilder("importCustomerJob", jobRepository)
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1(jobRepository, transactionManager))
                .end()
                .build();

    }


    /**
     * Bean configuration for the step.
     * This step processes customer data by reading from a CSV file, processing each item, and writing to the database.
     * <p>
     * the writer that writes customer data to the database
     *
     * @return Step
     */
    @Bean
    public Step step1(JobRepository jobRepository, PlatformTransactionManager transactionManager) {
        return new StepBuilder("step1", jobRepository)
                .<Customer, Customer>chunk(10, transactionManager)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

    /**
     * CustomerProcessor bean to process each Customer item.
     *
     * @return CustomerProcessor
     */
    @Bean
    public CustomerProcessor processor() {
        return new CustomerProcessor();
    }

}
