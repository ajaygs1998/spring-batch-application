### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

# Spring Batch Application

## Overview

This project demonstrates a basic setup and usage of Spring Batch with Spring Boot. It includes essential components like ItemReader, ItemProcessor, and ItemWriter, as well as configuration and execution of batch jobs.

## Project Setup

### Prerequisites

- JDK 22 
- Maven or Gradle
- IDE (IntelliJ IDEA, Eclipse, etc.)

### Using Spring Initializr

1. Go to [Spring Initializr](https://start.spring.io/).
2. Select **Maven Project** or **Gradle Project**.
3. Enter the `GroupId` and `ArtifactId`.
4. Choose the latest Spring Boot version.
5. Add dependencies: `Spring Web`, `Spring Batch`.
6. Click **Generate** to download the project and unzip it.

### Project Configuration

#### Spring Boot Application Class

```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBatchExampleApplication.class, args);
    }
}
